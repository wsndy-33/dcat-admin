<?php

namespace Dcat\Admin\Grid\Displayers;

class Input extends Editable
{
    protected $type = 'input';

    protected $view = 'admin::grid.displayer.editinline.input';
}
